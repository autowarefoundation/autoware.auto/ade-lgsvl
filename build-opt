#!/usr/bin/env bash
#
# Copyright 2018 Apex.AI, Inc.
# SPDX-License-Identifier: Apache-2.0

set -xe
LGSVL_VERSION="$1"; shift
ROS_VERSION="$1"; shift

cd "$(dirname "$(realpath "$0")")"

# Download and install LGSVL
wget https://github.com/lgsvl/simulator/releases/download/${LGSVL_VERSION}/svlsimulator-linux64-${LGSVL_VERSION}.zip
unzip svlsimulator-linux64-${LGSVL_VERSION}.zip
mv svlsimulator-linux64-${LGSVL_VERSION} _opt_lgsvl
chown root:root _opt_lgsvl -R
rm svlsimulator-linux64-${LGSVL_VERSION}.zip

# Change config to use ipv4
cp config.yml _opt_lgsvl/config.yml

# Add the sensor plugin
git clone https://gitlab.com/s-mlr/LgsvlInterruptSensor.git
mkdir -p _opt_lgsvl/AssetBundles/Sensors/
cp  LgsvlInterruptSensor/AssetBundles/Sensors/sensor_SimInterruptSensor _opt_lgsvl/AssetBundles/Sensors/sensor_SimInterruptSensor
rm -rf LgsvlInterruptSensor

# Add the Robotec.ai ROS2 Native Plugin
mkdir -p _opt_lgsvl/simulator_Data/Managed/
mkdir -p _opt_lgsvl/simulator_Data/Plugins/

wget https://github.com/RobotecAI/ROS2ForUnitySVLBridge/releases/download/svl-${LGSVL_VERSION}/ubuntu_${ROS_VERSION}_libs.zip
unzip ubuntu_${ROS_VERSION}_libs.zip
mv Plugins/Linux/x86_64/* _opt_lgsvl/simulator_Data/Plugins/
mv Plugins/*.dll _opt_lgsvl/simulator_Data/Managed/
rm -rf Plugins/ ubuntu_${ROS_VERSION}_libs.zip

# Environment Setup
cp env.sh _opt_lgsvl/.env.sh

# Use _opt_lgsvl for readability, rename to _opt b/c it's expected by Dockerfile
mv _opt_lgsvl _opt
